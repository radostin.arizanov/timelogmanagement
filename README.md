# TimeLogManagement



## Getting started

In order to start application successfully, configure the appropriate connection string for your SQL Server in appsettings.json.
Run "Update-Database" in PMC to apply migration for creating the DataBase.



## Dependencies


### Frameworks
* Microsoft.NETCore.App -v 8.0.1
* Microsoft.AspNETCore.App -v 8.0.1


### NuGet Packages
* Microsoft.EntityFrameworkCore.Proxies -v 8.0.2
* Microsoft.EntityFrameworkCore.SqlServer -v 8.0.2
* Microsoft.EntityFrameworkCore.Tools -v 8.0.2

