using Microsoft.AspNetCore.Mvc;
using TimeLogManagement.BLL.Services.DataServices;
using TimeLogManagement.BLL.Services.UserServices;
using TimeLogManagement.Common;
using TimeLogManagement.DAL;
using TimeLogManagement.DAL.Models;
using TimeLogManagement.UI.ViewModel;

namespace TimeLogManagement.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IDataService _dataService;
        private readonly IUserService _userService;

        public HomeController(ApplicationDbContext context,
                              IDataService dataService,
                              IUserService userService)
        {
            _context = context;
            _dataService = dataService;
            _userService = userService;
        }
        public IActionResult InitializeData()
        {
            _dataService.TruncateDbTables();
            _dataService.SeedDbProjects();
            _dataService.SeedDbUsers();

            return Ok();
        }

        public IActionResult UserInfo(int id)
        {
            var user = _userService.GetUserById(id);
            return Ok(user);
        }

        [HttpPost]
        public IActionResult LeftPanel(string startDate, string endDate)
        {
            var users = _context.Users.ToList();

            if (startDate != null || endDate != null)
            {
                try
                {
                    var parsedStaredDate = DateTime.Parse(startDate);
                    var parsedEndDate = DateTime.Parse(endDate);

                    users = _userService.FilterUsersByTimeRange(parsedStaredDate, parsedEndDate);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            var result = _userService.GetTop10UsersByTimeLog(users);
            return Ok(result);
        }

        public IActionResult Index(int? pageNumber, string sortOrder, string startDate, string endDate)
        {
            var users = _context.Users.ToList();

            DateTime parsedStaredDate = DateTime.MinValue;
            DateTime parsedEndDate = DateTime.MinValue;

            if (startDate != null || endDate != null)
            {
                try
                {
                    parsedStaredDate = DateTime.Parse(startDate);
                    parsedEndDate = DateTime.Parse(endDate);

                    users = _userService.FilterUsersByTimeRange(parsedStaredDate, parsedEndDate);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            switch (sortOrder)
            {
                case "name_asc":
                    users = users.OrderBy(u => u.FirstName).ToList();
                    break;
                case "name_desc":
                    users = users.OrderByDescending(u => u.FirstName).ToList();
                    break;
                default:
                    break;
            }

            var model = PageList<User>.Create(users, pageNumber ?? 1, Constants.PageSize);
            model.SortOrder = sortOrder;

            if (parsedStaredDate != DateTime.MinValue && parsedEndDate != DateTime.MinValue)
            {
                model.StartDate = parsedStaredDate.ToString();
                model.EndDate = parsedEndDate.ToString();
            }

            return View("./UI/Home/Index.cshtml", model);
        }
    }
}
