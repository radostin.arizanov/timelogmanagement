﻿using TimeLogManagement.DAL.Models;

namespace TimeLogManagement.BLL.Services.RandomServices
{
    public interface IRandomService
    {
        string GetRandomFirstName();

        string GetRandomLastName();

        string GetRandomDomain();

        string GetRandomProjectName();

        float GetRandomHour();

        DateTime GetRandomDate();

        List<TimeLog> GenerateRandomTimeLogs(User user, Project project);
    }
}
