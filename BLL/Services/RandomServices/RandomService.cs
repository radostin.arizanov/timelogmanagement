﻿using TimeLogManagement.DAL.Models;

namespace TimeLogManagement.BLL.Services.RandomServices
{
    public class RandomService : IRandomService
    {
        private static Random random = new();

        public string GetRandomFirstName()
        {
            var firstNamesList = new List<string> { "John", "Gringo", "Mark", "Lisa", "Maria", "Sonya", "Philip",
                "Jose", "Lorenzo", "George", "Justin" };

            int randomFirstNameIndex = random.Next(firstNamesList.Count);
            var randomFirstName = firstNamesList[randomFirstNameIndex];

            return randomFirstName;
        }

        public string GetRandomLastName()
        {
            var lastNamesList = new List<string> { "Johnson", "Lamas", "Jackson", "Brown", "Mason", "Rodriguez", "Roberts",
                "Thomas", "Rose", "McDonalds"};

            int randomLastNameIndex = random.Next(lastNamesList.Count);
            var randomLastName = lastNamesList[randomLastNameIndex];

            return randomLastName;
        }

        public string GetRandomDomain()
        {
            var domainNamesList = new List<string> { "hotmail.com", "gmail.com", "live.com" };

            int randomDomainNameIndex = random.Next(domainNamesList.Count);
            var randomDomain = domainNamesList[randomDomainNameIndex];

            return randomDomain;
        }

        public string GetRandomProjectName()
        {
            var projectNamesList = new List<string> { "My own", "Free Time", "Work" };

            int randomProjectNameIndex = random.Next(projectNamesList.Count);
            var randomProject = projectNamesList[randomProjectNameIndex];

            return randomProject;
        }

        public float GetRandomHour()
        {
            decimal numberInMinutes = random.Next(25, 480);
            decimal hours = numberInMinutes / 60;
            decimal minutes = numberInMinutes % 60;
            decimal result = (int)hours + (minutes / 100);

            return (float)result;
        }

        public DateTime GetRandomDate()
        {
            DateTime start = new DateTime(2000, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(random.Next(range));
        }

        public List<TimeLog> GenerateRandomTimeLogs(User user, Project project)
        {
            int randomNumber = random.Next(1, 20);
            List<TimeLog> result = new();

            for (int i = 0; i < randomNumber; i++)
            {
                TimeLog timeLog = new()
                {
                    Date = GetRandomDate(),
                    TotalHours = GetRandomHour(),
                    User = user,
                    Project = project
                };

                result.Add(timeLog);
            }

            return result;
        }
    }
}
