﻿using TimeLogManagement.DAL;
using TimeLogManagement.DAL.Models;
using TimeLogManagement.UI.ViewModel;

namespace TimeLogManagement.BLL.Services.UserServices
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _context;

        public UserService(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<UserViewModel> GetTop10UsersByTimeLog(List<User> input)
        {
            var users = input.OrderByDescending(u => u.TimeLogs.Sum(t => t.TotalHours)).Take(10).ToList();
            var result = new List<UserViewModel>();

            foreach (var user in users)
            {
                result.Add(new UserViewModel
                {
                    FullName = $"{user.FirstName} {user.LastName}",
                    TotalWorkedHours = user.TimeLogs.Sum(t => t.TotalHours),
                });
            }

            return result.ToList();
        }

        public UserViewModel GetUserById(int id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id);

            if (user == null)
            {
                throw new ArgumentException("User is null!");
            }

            var result = new UserViewModel
            {
                FullName = $"{user.FirstName} {user.LastName}",
                TotalWorkedHours = user.TimeLogs.Sum(t => t.TotalHours)
            };

            return result;
        }

        public List<User> FilterUsersByTimeRange(DateTime startingDate, DateTime endDate)
        {
            var result = _context.Users.Where(u => u.TimeLogs.All(t => t.Date >= startingDate && t.Date <= endDate));
            return result.ToList();
        }
    }
}
