﻿using TimeLogManagement.DAL.Models;
using TimeLogManagement.UI.ViewModel;

namespace TimeLogManagement.BLL.Services.UserServices
{
    public interface IUserService
    {
        List<UserViewModel> GetTop10UsersByTimeLog(List<User> input);

        UserViewModel GetUserById(int id);

        List<User> FilterUsersByTimeRange(DateTime startingDate, DateTime endDate);
    }
}
