﻿using Microsoft.EntityFrameworkCore;
using TimeLogManagement.BLL.Services.RandomServices;
using TimeLogManagement.DAL;
using TimeLogManagement.DAL.Models;

namespace TimeLogManagement.BLL.Services.DataServices
{
    public class DataService : IDataService
    {
        private readonly ApplicationDbContext _context;
        private readonly IRandomService _randomService;
        public DataService(ApplicationDbContext context,
                           IRandomService randomService)
        {
            _context = context;
            _randomService = randomService;
        }

        public void TruncateDbTables()
        {
            _context.TimeLogs.ExecuteDelete();
            _context.Users.ExecuteDelete();
            _context.Projects.ExecuteDelete();

            _context.SaveChanges();
        }

        public void SeedDbProjects()
        {
            _context.Projects.Add(new Project()
            {
                Name = "My own",
                TimeLogs = new List<TimeLog>(),
                Users = new List<User>()
            });
            _context.Projects.Add(new Project()
            {
                Name = "Free Time",
                TimeLogs = new List<TimeLog>(),
                Users = new List<User>()
            });
            _context.Projects.Add(new Project()
            {
                Name = "Work",
                TimeLogs = new List<TimeLog>(),
                Users = new List<User>()
            });

            _context.SaveChanges();
        }
        public void SeedDbUsers()
        {
            for (int i = 0; i < 100; i++)
            {
                var randomFirstName = _randomService.GetRandomFirstName();
                var randomLastName = _randomService.GetRandomLastName();
                var randomDomain = _randomService.GetRandomDomain();

                User user = new()
                {
                    FirstName = randomFirstName,
                    LastName = randomLastName,
                    Email = $"{randomFirstName}.{randomLastName}@{randomDomain}",
                    TimeLogs = new List<TimeLog>()
                };

                var project = _context.Projects.FirstOrDefault(x => x.Name == _randomService.GetRandomProjectName());
                var timeLogs = _randomService.GenerateRandomTimeLogs(user, project);

                foreach (var timeLog in timeLogs)
                {
                    project.TimeLogs.Add(timeLog);
                    user.TimeLogs.Add(timeLog);
                    _context.TimeLogs.Add(timeLog);
                }

                project.Users.Add(user);
                user.Project = project;
                _context.Users.Add(user);
                _context.SaveChanges();
            }
        }
    }
}
