﻿namespace TimeLogManagement.BLL.Services.DataServices
{
    public interface IDataService
    {
        void TruncateDbTables();

        void SeedDbProjects();

        void SeedDbUsers();
    }
}
