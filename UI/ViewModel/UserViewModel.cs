﻿namespace TimeLogManagement.UI.ViewModel
{
    public class UserViewModel
    {
        public string FullName { get; set; }

        public float TotalWorkedHours { get; set; }
    }
}
