﻿namespace TimeLogManagement.UI.ViewModel
{
    public class PageList<T> : List<T>
    {
        public PageList(List<T> items, int count, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            AddRange(items);
        }
        public int PageIndex { get; set; }

        public int TotalPages { get; set; }

        public string SortOrder { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public bool HasPreviousPage => PageIndex > 1;

        public bool HasNextPage => PageIndex < TotalPages;


        public static PageList<T> Create(List<T> source, int pageIndex, int pageSize)
        {
            var count = source.Count;
            var items = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            return new PageList<T>(items, count, pageIndex, pageSize);
        }
    }
}
