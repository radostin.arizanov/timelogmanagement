﻿namespace TimeLogManagement.DAL.Models
{
    public class Project
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public virtual ICollection<TimeLog> TimeLogs { get; set; }
    }
}
