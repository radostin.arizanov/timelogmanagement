﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TimeLogManagement.DAL.Models
{
    public class TimeLog
    {
        public int Id { get; set; }

        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }

        public DateTime Date { get; set; }

        public float TotalHours { get; set; }
    }
}
