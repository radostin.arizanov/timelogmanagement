﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TimeLogManagement.DAL.Models
{
    public class User
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        [ForeignKey("Project")]
        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }

        public virtual ICollection<TimeLog> TimeLogs { get; set; }
    }
}
